#!/bin/zsh
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
xcode-select --install
echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> ~/.zprofile
eval "$(/opt/homebrew/bin/brew shellenv)"
xargs brew install <"$(dirname "$0")/homebrew-essential.list"
curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh | zsh
sleep 1

mkdir ~/.config

zsh ~/.zplug/init.zsh
zsh "$(dirname "$0")/lazygit/init.sh"
zsh "$(dirname "$0")/nvim/init.sh"
zsh "$(dirname "$0")/zsh/init.sh"

ln "$(dirname "$0")/zsh/.zshrc" ~/.zshrc

mkdir ~/.config/wezterm
ln "$(dirname "$0")/wezterm/wezterm.lua" ~/.config/wezterm/wezterm.lua

# config default apps
# http://seriot.ch/resources/utis_graph/utis_graph.pdf
duti -s com.apple.QuickTimePlayerX public.audio all
duti -s company.thebrowser.Browser com.compuserve.gif all
duti -s com.cron.electron ics all

# git editor setting
git config --global core.editor  "nvim  -c 'startinsert'"
git config --global core.excludesfile ~/.gitignore
git config --global user.email "misaki@sarashino.io"
git config --global user.name "misaki sarashino"
git config --global merge.tool nvimdiff
echo '.DS_Store' > ~/.gitignore
# commit sign
git config --global gpg.format ssh
git config --global user.signingKey ~/.ssh/git_commit_sign
git config --global commit.gpgsign true
git config --global init.defaultBranch main

gh config set editor "nvim  -c 'startinsert'"

# Dock
cp "$(dirname "$0")/dock/com.apple.dock.plist" ~/Library/Preferences/com.apple.dock.plist
killall Dock

defaults write com.apple.finder AppleShowAllFiles -boolean true;

sudo /opt/homebrew/Cellar/podman/4.6.0/bin/podman-mac-helper install
podman machine init
podman machine start

echo "installation done."
print -Pn '
%F{red}[ ]%f restart or start terminal to install zplug things.
%F{red}[ ]%f restart or start nvim to install plugins.
%F{red}[ ]%f import raycast config '$(dirname "$0")'./raycast/Raycast*.rayconfig
%F{red}[ ]%f config proton email
%F{red}[ ]%f config bitwarden
%F{red}[ ]%f install "Source Han Sans Normal"
%F{red}[ ]%f install plasticity, typefully, ableton live.
%F{red}[ ]%f run: cp ./images/wezterm.icns /Applications/WezTerm.app/Contents/Resources/terminal.icns
%F{red}[ ]%f run: cp ./images/notion.icns /Applications/Notion.app/Contents/Resources/electron.icns
%F{red}[ ]%f run: cp ./images/cron.icns /Applications/Notion\ Calendar.app/Contents/Resources/icon.icns
'
