#!/bin/zsh

rm ~/.zshrc
ln "$(dirname "$0")/zsh/.zshrc" ~/.zshrc

rm ~/.config/wezterm/wezterm.lua
ln "$(dirname "$0")/wezterm/wezterm.lua" ~/.config/wezterm/wezterm.lua

rm ~/.config/nvim/init.vim
ln "$(dirname "$0")/nvim/init.vim" ~/.config/nvim/init.vim

rm ~/.config/lazygit/config.yml
ln "$(dirname "$0")/lazygit/config.yml" ~/.config/lazygit/config.yml
