echo ''
echo ' █     █░▓█████  ██▓     ▄████▄   ▒█████   ███▄ ▄███▓▓█████     ▄▄▄▄    ▄▄▄       ▄████▄   ██ ▄█▀'
echo '▓█░ █ ░█░▓█   ▀ ▓██▒    ▒██▀ ▀█  ▒██▒  ██▒▓██▒▀█▀ ██▒▓█   ▀    ▓█████▄ ▒████▄    ▒██▀ ▀█   ██▄█▒ '
echo '▒█░ █ ░█ ▒███   ▒██░    ▒▓█    ▄ ▒██░  ██▒▓██    ▓██░▒███      ▒██▒ ▄██▒██  ▀█▄  ▒▓█    ▄ ▓███▄░ '
echo '░█░ █ ░█ ▒▓█  ▄ ▒██░    ▒▓▓▄ ▄██▒▒██   ██░▒██    ▒██ ▒▓█  ▄    ▒██░█▀  ░██▄▄▄▄██ ▒▓▓▄ ▄██▒▓██ █▄ '
echo '░░██▒██▓ ░▒████▒░██████▒▒ ▓███▀ ░░ ████▓▒░▒██▒   ░██▒░▒████▒   ░▓█  ▀█▓ ▓█   ▓██▒▒ ▓███▀ ░▒██▒ █▄'
echo '░ ▓░▒ ▒  ░░ ▒░ ░░ ▒░▓  ░░ ░▒ ▒  ░░ ▒░▒░▒░ ░ ▒░   ░  ░░░ ▒░ ░   ░▒▓███▀▒ ▒▒   ▓▒█░░ ░▒ ▒  ░▒ ▒▒ ▓▒'
echo '  ▒ ░ ░   ░ ░  ░░ ░ ▒  ░  ░  ▒     ░ ▒ ▒░ ░  ░      ░ ░ ░  ░   ▒░▒   ░   ▒   ▒▒ ░  ░  ▒   ░ ░▒ ▒░'
echo '  ░   ░     ░     ░ ░   ░        ░ ░ ░ ▒  ░      ░      ░       ░    ░   ░   ▒   ░        ░ ░░ ░ '
echo '    ░       ░  ░    ░  ░░ ░          ░ ░         ░      ░  ░    ░            ░  ░░ ░      ░  ░   '
echo '                        ░                                            ░           ░               '
echo ''

# path
export GOPATH=~/go
export PATH=$GOPATH/bin:$PATH
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && \. "/opt/homebrew/opt/nvm/nvm.sh" # This loads nvm
[ -s "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm" ] && \. "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion
export GPG_TTY=$(tty)
export CONTAINERS_MACHINE_PROVIDER=applehv

# export PS1=$'%F{magenta}%n> %~%f \$\n'
export LANG=en_US.UTF-8
# for vim
stty stop undef
stty start undef
alias e=nvim
alias vi=nvim
alias vim=nvim
# end for vim
alias docker=podman
alias ls="eza -a"
alias ll="eza -al"
alias tree="eza -al --tree -I '.git' --git-ignore "
alias wttr="curl 'wttr.in?format=v2' | less -R"
alias obs='f(){ 
local folder="00_any"
if [[ -z "$2" ]]
then
	if [[ -z "$1" ]]
	then
		(cd ~/Documents/obsidian && e -c "NERDTreeToggle")
	else
		e -c "ObsidianNew $folder/$1"
	fi
else
	[[ "$1" == "brain" ]] && folder="01_brain"
	[[ "$1" == "diary" ]] && folder="02_diary"
	[[ "$1" == "career" ]] && folder="03_career"
	[[ "$1" == "projects" ]] && folder="04_projects"
	[[ "$1" == "public" ]] && folder="05_public"
	e -c "ObsidianNew $folder/$1"
fi
unset -f f
}; f'
alias today='e -c "ObsidianToday"'

zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' special-dirs true
autoload -Uz compinit && compinit
autoload -Uz chpwd_recent_dirs cdr add-zsh-hook
add-zsh-hook chpwd chpwd_recent_dirs

export HISTFILESIZE=1000000000
export HISTSIZE=1000000000
setopt INC_APPEND_HISTORY
export HISTTIMEFORMAT="[%F %T] "
setopt EXTENDED_HISTORY
setopt HIST_FIND_NO_DUPS
setopt HIST_IGNORE_ALL_DUPS

# zplug area below
source ~/.zplug/init.zsh

zplug "chrissicool/zsh-256color"
zplug "plugins/git",   from:oh-my-zsh
zplug "mollifier/anyframe"
zplug "b4b4r07/httpstat", \
	as:command, \
	use:'(*).sh', \
	rename-to:'$1'

zplug "woefe/git-prompt.zsh"
zplug "b4b4r07/emoji-cli"

if ! zplug check --verbose; then
	printf "Install? [y/N]: "
	if read -q; then
		echo; zplug install
	fi
fi
zplug load
compinit

# zplug "woefe/git-prompt.zsh" config
ZSH_GIT_PROMPT_FORCE_BLANK=1
ZSH_GIT_PROMPT_SHOW_STASH=1
ZSH_GIT_PROMPT_SHOW_UPSTREAM="symbol"

ZSH_THEME_GIT_PROMPT_PREFIX="%F{yellow}%f%B %b["
ZSH_THEME_GIT_PROMPT_SUFFIX="]"
ZSH_THEME_GIT_PROMPT_SEPARATOR="|"
ZSH_THEME_GIT_PROMPT_BRANCH="%{$fg[magenta]%}"
ZSH_THEME_GIT_PROMPT_UPSTREAM_SYMBOL=" %{$fg_bold[yellow]%}⟳ "
ZSH_THEME_GIT_PROMPT_UPSTREAM_PREFIX="%{$fg[yellow]%} ⤳ "
ZSH_THEME_GIT_PROMPT_UPSTREAM_SUFFIX=""
ZSH_THEME_GIT_PROMPT_DETACHED="%{$fg_no_bold[cyan]%}:"
ZSH_THEME_GIT_PROMPT_BEHIND="%{$fg_no_bold[cyan]%}↓"
ZSH_THEME_GIT_PROMPT_AHEAD="%{$fg_no_bold[cyan]%}↑"
ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg[red]%}✖"
ZSH_THEME_GIT_PROMPT_STAGED="%{$fg[green]%}●"
ZSH_THEME_GIT_PROMPT_UNSTAGED="%{$fg[red]%}✚"
ZSH_THEME_GIT_PROMPT_UNTRACKED="…"
ZSH_THEME_GIT_PROMPT_STASHED="%{$fg[blue]%}⚑"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg_bold[green]%}✔"

PROMPT='%F{blue}%n %F{magenta}󱥸 %F{white}%~%f $(gitprompt)
%F{cyan}%*%(12V.%F{green}%12v%f .)%(?.%F{magenta}.%F{red})❯%f '
RPROMPT=''
# pnpm
export PNPM_HOME="$HOME/Library/pnpm"
export PATH="$PNPM_HOME:$PATH"
# pnpm end

export SSH_AUTH_SOCK=$HOME/Library/Containers/com.maxgoedjen.Secretive.SecretAgent/Data/socket.ssh

# lazygit
export XDG_CONFIG_HOME=$HOME/.config
