local wezterm = require 'wezterm';

local default_window_padding = {
	left = '40pt',
	right = '40pt',
	top = '30pt',
	bottom = '30pt',
}

local default_font_size = 11.5

local default_font_family = wezterm.font_with_fallback {
	{ family = "SauceCodePro Nerd Font Mono" },
	{ family = "Source Han Sans Normal" },
}

wezterm.on('update-right-status', function(window, pane)
	local overrides = window:get_config_overrides() or {}
	local process_name = pane:get_foreground_process_name()
	local nvim = '/nvim'
	local zsh = '/zsh'
	local zen_mode = wezterm.GLOBAL.zen_mode
	if (not zen_mode) then
		if process_name:sub(- #nvim) == nvim then
			overrides.window_padding = nil
		else
			overrides.window_padding = default_window_padding
		end
	end
	window:set_config_overrides(overrides)
	if process_name:sub(- #zsh) == zsh then
		window:set_right_status(wezterm.format {
			{ Text = '' },
		})
	else
		window:set_right_status(wezterm.format {
			{ Text = process_name .. '   ' },
		})
	end
end)

wezterm.GLOBAL.zen_mode = wezterm.GLOBAL.zen_mode or false
wezterm.on('augment-command-palette', function(window, pane)
	return {
		{
			brief = 'Toggle Zen Mode',
			icon = 'pom_clean_code',

			action = wezterm.action_callback(function(window, pane, line)
				local zen_mode = not wezterm.GLOBAL.zen_mode
				wezterm.GLOBAL.zen_mode = zen_mode
				local dimensions = window:get_dimensions()
				if (zen_mode and not dimensions.is_full_screen) or
						((not zen_mode) and dimensions.is_full_screen) then
					window:toggle_fullscreen()
				end

				local overrides = window:get_config_overrides() or {}
				if zen_mode then
					local side_margin = (dimensions.pixel_width - 1680) / 2
					local top_margin = (dimensions.pixel_height - 1890) / 2
					overrides.font_size = 32.0
					overrides.window_padding = {
						left = side_margin,
						right = side_margin,
						top = top_margin,
						bottom = '0pt',
					}
					overrides.font = wezterm.font_with_fallback {
						{ family = "SauceCodePro Nerd Font Mono" },
						{ family = "FOT-TsukuMin Pr6" },
					}
					overrides.tab_bar_at_bottom = true
				else
					overrides.font_size = default_font_size
					overrides.window_padding = default_window_padding
					overrides.font = default_font_family
					overrides.tab_bar_at_bottom = false
				end
				window:set_config_overrides(overrides)
			end),
		},
	}
end)

return {
	status_update_interval = 50,
	font = default_font_family,
	keys = {
		{
			key = "k",
			mods = "CMD",
			action = wezterm.action.ActivateCommandPalette,
		},
	},
	initial_cols = 360,
	initial_rows = 100,
	window_decorations = "RESIZE | MACOS_FORCE_ENABLE_SHADOW",
	window_background_opacity = 0.95,
	macos_window_background_blur = 16,
	native_macos_fullscreen_mode = true,
	use_ime = true,
	font_size = default_font_size,
	color_scheme = "Solarized (light) (terminal.sexy)",
	command_palette_bg_color = "#586e75",
	command_palette_fg_color = "#eee8d5",
	command_palette_font_size = 24,
	adjust_window_size_when_changing_font_size = false,
	canonicalize_pasted_newlines = "CarriageReturnAndLineFeed",
	check_for_updates = true,
	check_for_updates_interval_seconds = 86400,
}
