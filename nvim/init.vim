" .config/nvim/init.vim
" Plugins will be downloaded under the specified directory.
call plug#begin(has('nvim') ? stdpath('data') . '/plugged' : '~/.vim/plugged')
Plug 'djoshea/vim-autoread'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'williamboman/mason.nvim'
Plug 'williamboman/mason-lspconfig.nvim'
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-path'
Plug 'dmitmel/cmp-cmdline-history'
" pynvim required; several formatter are exepected to be installed.
Plug 'Chiel92/vim-autoformat'
Plug 'akinsho/toggleterm.nvim', {'tag' : '*'}
Plug 'numToStr/Comment.nvim'
Plug 'mattn/emmet-vim'
Plug 'preservim/nerdtree'
Plug 'ryanoasis/vim-devicons'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'nvim-lualine/lualine.nvim'
Plug 'airblade/vim-gitgutter'
Plug 'kylechui/nvim-surround'
Plug 'windwp/nvim-autopairs'
Plug 'brenoprata10/nvim-highlight-colors'
Plug 'ericbn/vim-solarized'
Plug 'vim-skk/eskk.vim'
Plug 'rbgrouleff/bclose.vim'
Plug 'tkhren/vim-fake'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'epwalsh/obsidian.nvim'
Plug 'kdheepak/lazygit.nvim'
Plug 'dinhhuy258/git.nvim'
Plug 'sarashino/vim-emoji'
" List ends here. Plugins become visible to Vim after this call.
call plug#end()

" plugin config

let mapleader = " "

" 'preservim/nerdtree'
let g:NERDTreeMapOpenInTab = '<C-t>'
let g:NERDTreeWinPos = "left"
let NERDTreeShowHidden=0
let NERDTreeIgnore = ['\.pyc$', '__pycache__', '.git']
let g:NERDTreeWinSize=35
map <leader>nt :NERDTreeToggle<cr>
map <leader>nf :NERDTreeFind<cr>

" 'numToStr/Comment.nvim'
lua << EOF
require('Comment').setup()
EOF

" 'nvim-lualine/lualine.nvim'
lua << END
require('lualine').setup{
options = {
	theme = 'solarized_light',
	globalstatus = true
	},
sections = {
	lualine_c = {
		{
				'filename',
				file_status = true,
				newfile_status = true,
				path = 4,

				symbols = {
					modified = '[+]',
					readonly = "🔒",
					unnamed = '[No Name]',
					newfile = '[New]',
				}
				}
		}
	}
}
END

" 'kylechui/nvim-surround'
lua require("nvim-surround").setup()
" 'windwp/nvim-autopairs'
lua << EOF
require("nvim-autopairs").setup {map_c_h = true}
EOF

" 'ericbn/vim-solarized'
set termguicolors
set background=light
colorscheme solarized

" 'brenoprata10/nvim-highlight-colors'
set t_Co=256
lua <<EOF
require('nvim-highlight-colors').setup {}
EOF

" 'airblade/vim-gitgutter'
highlight SignColumn ctermbg=none
highlight GitGutterAdd ctermfg=green
highlight GitGutterChange ctermfg=cyan
highlight GitGutterDelete ctermfg=red
highlight GitGutterChangeDelete ctermfg=red

let g:gitgutter_sign_added = ''
let g:gitgutter_sign_modified = ''
let g:gitgutter_sign_removed = ''
let g:gitgutter_sign_removed_first_line = ''
let g:gitgutter_sign_removed_above_and_below = ''
let g:gitgutter_sign_modified_removed = ''

set updatetime=250

" 'Chiel92/vim-autoformat'
let g:formatdef_eslint = '"eslint_d --stdin --stdin-filename=".expand("%:p")." --fix-to-stdout"'
let g:formatters_typescript = ['prettier', 'eslint']
let g:formatters_typescriptreact = ['prettier', 'eslint']
let g:formatters_javascript = ['prettier', 'eslint']
let g:formatters_javascriptreact = ['prettier', 'eslint']
nnoremap <leader>f :Autoformat<CR>:WatchForChangesAllFile<CR>
" au BufWrite * :Autoformat
" Plug 'williamboman/mason.nvim'
" Plug 'williamboman/mason-lspconfig.nvim'
" Plug 'neovim/nvim-lspconfig'
lua <<EOF
require("mason").setup()
require("mason-lspconfig").setup{
automatic_installation = true
}
require("mason-lspconfig").setup_handlers {
	function (server_name)
		local capabilities = require('cmp_nvim_lsp').default_capabilities()
		require("lspconfig")[server_name].setup {
			capabilities = capabilities 
		}
		end,
}
vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(
vim.lsp.handlers.hover, {
	border = 'single'
}
)
vim.diagnostic.config{
float={border='single'}
}
EOF

nnoremap <leader>f :call <SID>format()<CR>

function! s:format()
lua <<EOF
local clients = vim.lsp.get_active_clients()
if next(clients) == nil then
	vim.cmd('Autoformat')
else
	vim.lsp.buf.format()
	end
EOF
endfunction

nnoremap <C-k> :lua vim.lsp.buf.hover()<CR>
nnoremap <C-,> :lua vim.diagnostic.goto_next()<CR>
nnoremap <C-'> :lua vim.diagnostic.goto_prev()<CR>
nnoremap <C-d> :lua vim.lsp.buf.references()<CR>
nnoremap jd    :lua vim.lsp.buf.definition()<CR>
nnoremap ji    :lua vim.lsp.buf.implementation()<CR>
nnoremap jt    :lua vim.lsp.buf.type_definition()<CR>

" 'hrsh7th/cmp-nvim-lsp'
" 'hrsh7th/nvim-cmp'
" 'hrsh7th/cmp-cmdline'
lua <<EOF
local cmd = require'cmp';
local window = require('cmp.config.window')
local mapping = require('cmp.config.mapping')
local sources = require('cmp.config.sources')
local config = require('cmp.config')
cmd.setup{
window = {
	completion = window.bordered(),
	documentation = window.bordered(),
},
mapping = {
	["<C-p>"] = mapping.select_prev_item(),
	["<C-n>"] = mapping.select_next_item(),
	['<C-c>'] = function()
			mapping.abort()
			vim.cmd('stopinsert')
		end,
	["<Tab>"] = mapping.confirm { select = true },
},
sources = {
	{ name = "nvim_lsp" },
},
}
cmd.setup.cmdline(":",
{
	mapping = {
		["<C-p>"] = {
			c = function()
			local cmp = require('cmp')
			if cmp.visible() then
				cmp.select_next_item()
			else
				cmp.complete()
				end
				end,
				},
		["<C-n>"] =  {
			c = function()
			local cmp = require('cmp')
			if cmp.visible() then
				cmp.select_prev_item()
			else
				cmp.complete()
				end
				end
				},
		['<C-c>'] = mapping.abort(),
		["<Tab>"] = mapping.confirm { select = true },
		},
		view = {
			entries = { name = 'custom', selection_order = 'bottom_up' }
			},
		sources = sources({
		{ name = 'cmdline_history' },
		{ name = 'path' }
		}, {
			{ name = 'cmdline' }
		}
		)
})
EOF

" 'nvim-treesitter/nvim-treesitter'
lua << EOF
require'nvim-treesitter.configs'.setup {
	auto_install = true,
	ignore_install = {},
	highlight = {
		enable = true,
		disable = function(lang, buf)
		local max_filesize = 100 * 1024 -- 100 KB
		local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
		if ok and stats and stats.size > max_filesize then
			return true
			end
			end,
			additional_vim_regex_highlighting = false,
	},
}
EOF

" 'nvim-telescope/telescope.nvim'
nnoremap <Leader><Leader> <cmd>Telescope find_files<cr>

" 'djoshea/vim-autoread'
set autoread
au FocusGained * WatchForChangesAllFile!
au VimEnter * WatchForChangesAllFile!

" akinsho/toggleterm.nvim', {'tag' : '*'}
lua <<EOF
require("toggleterm").setup{
direction = 'float'
}
EOF
nnoremap <leader>term :ToggleTerm<CR>

" 'preservim/tagbar'
command! CTags TagbarToggle<CR>

" 'vim-skk/eskk.vim'
let g:eskk#egg_like_newline = 1
let g:eskk#auto_save_jisyo_at_exit = -1
let g:eskk#large_dictionary = {'path': '~/Library/Application\ Support/AquaSKK/SKK-JISYO.L','sorted': 1,'encoding': 'euc-jp'}
set encoding=utf-8
set fileencoding=utf-8
augroup vimrc_eskk
  autocmd!
  autocmd User eskk-enable-post lmap <buffer> l <Plug>(eskk:disable)
  autocmd User eskk-enable-post lmap <buffer> <C-j>   <Plug>(eskk:enable)
  autocmd User eskk-enable-post lmap <buffer> <C-e> <End>
augroup END


" 'epwalsh/obsidian.nvim'
lua <<EOF
require("obsidian").setup({
	dir = "~/Library/Mobile Documents/iCloud~md~obsidian/Documents/obsidian",
  daily_notes = {
    folder = "02_diary",
  },
	note_id_func = function(title)
    local suffix = ""
		if title ~= nil then
			local title = title:gsub(" ", "-"):gsub("[^A-Za-z0-9-]", ""):lower()
			return title
		else
			for _ = 1, 4 do
				suffix = suffix .. string.char(math.random(65, 90))
				end
		end
    return tostring(os.date('%Y-%m-%dT%H:%M:%S')) .. "--" .. suffix
  end,
	open_app_foreground = true,
})
EOF

" 'kdheepak/lazygit.nvim'
nnoremap <silent> <leader>gg :LazyGit<CR>
let g:lazygit_use_custom_config_file_path = 1
let g:lazygit_config_file_path = '~/.config/lazygit/config.yml'

" 'dinhhuy258/git.nvim'
lua <<EOF
require('git').setup({
  default_mappings = true, -- NOTE: `quit_blame` and `blame_commit` are still merged to the keymaps even if `default_mappings = false`

  keymaps = {
    -- Open blame window
    blame = "<Leader>gb",
    -- Close blame window
    quit_blame = "q",
    -- Opens a new diff that compares against the current index
    diff = "<Leader>gd",
    -- Close git diff
    diff_close = "q",
  },
})
EOF

" 'tkhren/vim-fake'
call fake#define('name', 'fake#int(1) ? fake#gen("male_name")'
			\ . ' : fake#gen("female_name")')
call fake#define('firstname', 'fake#gen("name")')
call fake#define('lastname', 'fake#gen("surname")')
call fake#define('fullname', 'fake#gen("name") . " " . fake#gen("surname")')
call fake#define('sentense', 'fake#capitalize('
			\ . 'join(map(range(fake#int(3,15)),"fake#gen(\"nonsense\")"))'
			\ . ' . fake#chars(1,"..............!?"))')
call fake#define('paragraph', 'join(map(range(fake#int(3,10)),"fake#gen(\"sentense\")"))')
call fake#define('lipsum', 'fake#gen("paragraph")')
call fake#define('birthday', 'strftime("%FT%T", localtime() - fake#int(0, 60*60*24*365.25 * 120))')
call fake#define('birthdayz', 'strftime("%FT%T%z", localtime() - fake#int(0, 60*60*24*365.25 * 120))')
call fake#define('url', '"http" . fake#choice(["s",""]) . "://" . '
			\ . 'join(map(range(fake#int(1,3)),"fake#gen(\"nonsense\")"),".") . '
			\ . '"." . fake#gen("gtld")')
call fake#define('email', 'fake#gen("nonsense") . "@" . '
			\ . 'join(map(range(fake#int(1,2)),"fake#gen(\"nonsense\")"),".") . '
			\ . '"." . fake#gen("gtld")')
command -range -nargs=? Dummy
			\ if <q-args> == '' |
			\ for dummy in ['name', 'surname', 'firstname', 'lastname', 'fullname', 'sentense', 'paragraph', 'lipsum', 'birthday', 'birthdayz', 'url', 'email', 'country', 'gtld', 'job', 'word', 'nonsense'] |
			\ execute "silent! " . <line1> . "," . <line2> . "s/\\<_" . dummy . "\\>/\\=fake#gen('" . dummy . "')/g" |
			\ endfor |
			\ else |
			\ <line1>,<line2>s/\<_dummy\>/\=fake#gen(<f-args>)/g |
			\ endif

" 'sarashino/vim-emoji'
let g:return_emoji=1
set completefunc=emoji#complete

" gf as like usual click.
let s:gf_ext_file_handlers = {
			\'heic': 'Open',
			\'jpeg': 'Open',
			\'jpg': 'Open',
			\'png': 'Open',
			\'gif': 'Open',
			\'mp3': 'Open',
			\'m4a': 'Open',
			\'wav': 'Open',
			\'mp4': 'Open',
			\'mov': 'Open',
			\'pdf': 'Open'
			\}
function! s:Open(arg)
	if has('macunix')
		exec '!open "' . a:arg . '"'
	elseif has('unix')
		exec '!xdg-open "' . a:arg . '"'
	else
		echoerr 'are u serious?'
	endif
endfunction
command! -nargs=1 Open call <SID>Open(<f-args>)
function! s:click(uri)
	if empty(a:uri)
		echoerr "E446: Nothing under cursor"
	endif
	let url = matchstr(a:uri, '^http[s]\?:\/\/[[:alnum:]%\/_#.-]*$')
	if !empty(url)
		exec 'Open ' . a:uri
	else
		let ext = tolower(matchstr(a:uri, '.[[:alnum:]]*$')[1:])
		let handler = get(s:gf_ext_file_handlers, ext)
		if !empty(handler)
			exe handler . " " . a:uri
		else
			exec "normal! \<C-w>gf"
		endif
	endif
endfunction

nmap gf :<C-u>call <SID>click(expand("<cfile>"))<CR>

" not for plugins
command! WQ x
command! Wq x
command! W w
command! Q q
command! X x

noremap E ge
cnoreabbrev nor normal
cnoreabbrev r normal .
nnoremap q: <nop>
nnoremap Q <nop>

set shiftwidth=2
set tabstop=2

" mkdir -p on write
lua <<EOF
vim.api.nvim_create_autocmd({ "BufWritePre" }, {
	pattern = "*",
	group = vim.api.nvim_create_augroup("auto_create_dir", { clear = true }),
	callback = function(ctx)
	local dir = vim.fn.fnamemodify(ctx.file, ":p:h")
	vim.fn.mkdir(dir, "p")
	end
})
EOF

" visual mode asterisk search
xnoremap * :<C-u>call <SID>VSetSearch()<CR>/<C-R>=@/<CR><CR>
xnoremap # :<C-u>call <SID>VSetSearch()<CR>?<C-R>=@/<CR><CR>

function! s:VSetSearch()
	let temp = @s
	norm! gv"sy
	let @/ = '\V' . substitute(escape(@s, '/\'), '\n', '\\n','g')
	let @s = temp
endfunction

set wildmode=longest,list
set mouse=a
set ignorecase

" yank always use os clipboard
function s:YankOrCut(event)
	if !empty(a:event.regname)
		return
	endif
	if a:event.operator == 'y'
		call setreg('*', a:event.regcontents)
	endif
endfunction
augroup yank_or_cut
	au!
	au TextYankPost * call <SID>YankOrCut(v:event)
augroup END

" boilerplates
autocmd BufNewFile *.html 0r ~/.config/nvim/boilerplates/html/index.html

" for dvorak
function! MapDvorak()
	" for dvorak
	noremap k n
	noremap K N

	noremap t <Up>
	noremap n <Down>
	noremap T <Up>
	noremap N <Down>
	noremap s <Right>
	noremap - s
	noremap _ S
	" for dvorak
endfunction

call MapDvorak()

map <leader>tab :tabnew<CR>
map <leader>tn :tabnext<CR>
map <leader>tp :tabprevious<CR>
map <leader>th :tabprevious<CR>
" previously opened buffer
map <leader>p :b#<CR>

command! VTerm vs|term
command! HTerm sp|term
map <leader>c :sp<CR><C-n>:term<CR>i
map <leader>vc :VTerm<CR>i
map <leader>hc :HTerm<CR>i

tnoremap <C-w> <C-\><C-n>:bd!<CR>
tnoremap <C-\><C-\> <C-\><C-n>

" emacs binding
nnoremap <C-h> <C-w>h
nnoremap <C-t> <C-u>
nnoremap <C-n> <C-d>
nnoremap <C-s> <C-w>l
nnoremap <C-w> :bd<CR>
nnoremap <C-]> gt

nmap <C-p> <Up>
" conflict with window move
" nmap <C-n> <Down>
nmap <C-b> <Left>
nmap <C-f> <Right>

imap <C-c> <Esc>
imap <C-p> <Up>
imap <C-n> <Down>
imap <C-b> <Left>
imap <C-f> <Right>
imap <C-a> <C-o>:call <SID>home()<CR>
imap <C-e> <End>
imap <C-d> <Del>
imap <C-h> <BS>
imap <C-k> <C-r>=<SID>kill()<CR>
imap <C-t> <C-r>=<SID>swap()<CR>

cmap <C-p> <Up>
cmap <C-n> <Down>
cmap <C-b> <Left>
cmap <C-f> <Right>
cmap <C-a> <Home>
cmap <C-e> <End>
cmap <C-d> <Del>
cmap <C-h> <BS>
cmap <expr> <C-k> <SID>cmd_kill()
cmap <expr> <C-t> <SID>cmd_swap()
cnoremap : <C-r>=<SID>call_command_map()<CR>

" dev container
function! s:DevContainer(args)
	let l:path = getcwd()
	if isdirectory(l:path . "/.devcontainer")
		if a:args == ''
			:!pnpx @devcontainers/cli up --workspace-folder=.
		else
			:exec("!pnpx @devcontainers/cli exec --workspace-folder=. " . a:args)
		endif
	else
		echoerr '.devcontainer not found.'
	endif
endfunction
command -nargs=* -complete=shellcmd DevContainer call <SID>DevContainer(<q-args>)
command StopDevContainer !pnpx @devcontainers/cli stop --workspace-folder=.

function! s:call_command_map()
	if getcmdpos() == 1 && getcmdtype() == ":"
		let l:path = getcwd()
		if isdirectory(l:path . "/.devcontainer")
			return 'DevContainer '
		else
			return '!'
		endif
	endif
	return ':'
endfunction

function! s:home()
	let start_column = col('.')
	normal! ^
	if col('.') == start_column
		: normal! 0
	endif
	return ''
endfunction

function! s:kill()
	let [text_before, text_after] = s:get_splited_line()
	if len(text_after) == 0
		: normal! J
	else
		: call setline(line('.'), text_before)
	endif
	return ''
endfunction

function! s:swap()
	let [text_before, text_after] = s:get_splited_line()
	if (len(text_before) > 0)
		if (len(text_after) > 0)
			let before_letter =  text_before[-1:-1]
			let after_letter =  text_after[0:0]
			:call setline(line('.'), text_before[0:-2] . after_letter . before_letter . text_after[1:-1])
			:call cursor( line('.'), col('.')+1)
		else
			let before_letter =  text_before[-2:-2]
			let after_letter =  text_before[-1:-1]
			:call setline(line('.'), text_before[0:-3] . after_letter . before_letter)
		endif
	endif
	return ''
endfunction

function! s:get_splited_line()
	let line_text = getline(line('.'))
	let text_after  = line_text[col('.')-1 :]
	let text_before = (col('.') > 1) ? line_text[: col('.')-2] : ''
	return [text_before, text_after]
endfunction

function! s:cmd_kill()
	let [text_before, text_after] = s:get_cmd_splited_line()
	let cmd = ""
	let l = len(text_after)
	let c = 0
	while c < l
		let cmd .= "\<Del>"
		let c += 1
	endwhile
	return cmd
endfunction

function! s:cmd_swap()
	let [text_before, text_after] = s:get_cmd_splited_line()
	if (len(text_before) > 0)
		if (len(text_after) > 0)
			let before_letter =  text_before[-1:-1]
			let after_letter =  text_after[0:0]
			return "\<BS>\<Del>".after_letter.before_letter
		else
			let before_letter =  text_before[-2:-2]
			let after_letter =  text_before[-1:-1]
			return "\<BS>\<Del>".after_letter.before_letter
		endif
	endif
	return ''
	return s:swap(getcmdline())
endfunction

function! s:get_cmd_splited_line()
	let line_text = getcmdline()
	let text_after  = line_text[getcmdpos()-1 :]
	let text_before = (getcmdpos() > 1) ? line_text[: getcmdpos()-2] : ''
	return [text_before, text_after]
endfunction
