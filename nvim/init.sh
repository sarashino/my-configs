#!/bin/zsh

sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
brew install python
brew install python3
sudo ln /opt/homebrew/bin/python3 /usr/local/bin/python3
brew install yamllint
sudo gem install neovim
pip2 install pynvim
pip3 install pynvim
pip3 install black
npm install -g js-beautify
npm install -g eslint_d
npm install -g stylelint stylelint-config-standard
npm install -g prettier
gem install sass
go install mvdan.cc/gofumpt@latest
npm install -g remark-cli
pip install sqlparse
brew install lazygit

mkdir -p ~/.config/nvim
ln "$(dirname "$0")/init.vim" ~/.config/nvim/init.vim
mkdir -p ~/.config/nvim/boilerplates/html
ln "$(dirname "$0")/boilerplates/html/index.html" ~/.config/nvim/boilerplates/html/index.html

